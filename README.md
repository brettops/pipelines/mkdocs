# MkDocs Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/mkdocs/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/mkdocs/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Build documentation sites with [MkDocs](https://www.mkdocs.org/).

## Usage

### Project Setup

At a minimum, this pipeline requires that:

- A `mkdocs.yml` file is created

- A `docs/` directory exists and contains some Markdown files

### Using The Pipeline

Include the pipeline in the `.gitlab-ci.yml` file:

```yaml
include:
  - project: brettops/pipelines/mkdocs
    file: include.yml
```
